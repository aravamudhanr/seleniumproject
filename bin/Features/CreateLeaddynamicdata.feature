Feature: Create Lead
Background:
Given Open the browser 
And Max the browser
And Set the TimeOut  
And Launch the URL

Scenario Outline: Positive Login
And Enter the UserName as <username> 
And Enter the Password as <password> 
And Click on the Login Button
And Click on the CRM Link 
And Click on the Leads 
And Click on the Create Lead 
And Enter the Company Name as TCS 
And Enter the First Name as Ara 
And Enter the Last Name as R 
When Click on Create Lead Button

Examples:
|username|password|
|demosalesmanager|crmsfa|


