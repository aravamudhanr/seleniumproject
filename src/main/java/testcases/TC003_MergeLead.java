package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC003_MergeLead extends ProjectMethods{
	@BeforeTest
	public void setData() {
		testCaseName = "TC003_MergeLead";
		testDescription = "MergeLead";
		authors = "Ara";
		category = "smoke";
		dataSheetName = "TC003 MergeLead";
		testNodes = "Leads";
	}
	
	@Test(dataProvider = "fetchData")
	public void login(String userName,String password, String fromname, String toname) throws InterruptedException {		
		new LoginPage().enterUserName(userName).enterPassword(password).clickLogin()
		.clickcrm().clickleadtab().clickmergelead().FromLead().TypeFirstName(fromname).ClickFindLeads()
		.SelectLeadID().ToLead().TypeToFirstName(toname).ClickToFindLeads().SelectToLeadID().ClickMerge();
		
				
		/*LoginPage lp = new LoginPage();
		lp.enterUserName();
		lp.enterPassword();
		lp.clickLogin();*/
	}
	
}
