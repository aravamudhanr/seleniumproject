package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC002_CreateLead extends ProjectMethods{
	@BeforeTest
	public void setData() {
		testCaseName = "TC002_CreateLead";
		testDescription = "CreateLead";
		authors = "Ara";
		category = "smoke";
		dataSheetName = "TC002 CreateLead";
		testNodes = "Leads";
	}
	
	@Test(dataProvider = "fetchData")
	public void login(String userName,String password, String cname, String fname, String lname) {		
		new LoginPage().enterUserName(userName).enterPassword(password).clickLogin()
		.clickcrm().clickleadtab().clickcreatelead().typecomname(cname).typefname(fname).typelname(lname)
		.clickcreate().verifyfname(fname);
		
				
		/*LoginPage lp = new LoginPage();
		lp.enterUserName();
		lp.enterPassword();
		lp.clickLogin();*/
	}
	
}
