package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class CreateLeadPage extends ProjectMethods{

	public CreateLeadPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.LINK_TEXT, using = "Create Lead")
	WebElement elecreatelead;
	@FindBy(how = How.LINK_TEXT, using = "Merge Leads")
	WebElement elemergelead;
	
	
	public CreateLeadDataPage clickcreatelead()
	{
		click(elecreatelead);
		return new CreateLeadDataPage();
	}
	
	public MergeLeadDataPage clickmergelead()
	{
		click(elemergelead);
		return new MergeLeadDataPage();
	}
	
}
