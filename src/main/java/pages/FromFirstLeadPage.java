package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class FromFirstLeadPage extends ProjectMethods{

	public FromFirstLeadPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "//input[@name='firstName']")
	WebElement elewinfirstname;
	@FindBy(how = How.XPATH, using = "//button[text()='Find Leads']")
	WebElement elefindlead;
	@FindBy(how = How.XPATH, using = "((//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]")
	WebElement eleselectfirst;	
	/*public void SwitchFromWindow()
	{
		switchToWindow(1);
		return new 
	}*/
	
	public FromFirstLeadPage TypeFirstName(String data)
	{
		switchToWindow(1);
		type(elewinfirstname, data);
		return this;
	}
	public FromFirstLeadPage ClickFindLeads() throws InterruptedException
	{
		click(elefindlead);
		Thread.sleep(3000);
		return this;
	}
	public MergeLeadDataPage SelectLeadID()
	{
		click(eleselectfirst);
		switchToWindow(0);
		return new MergeLeadDataPage();
	}
}
