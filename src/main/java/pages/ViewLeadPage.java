package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class ViewLeadPage extends ProjectMethods{

	public ViewLeadPage() {
		PageFactory.initElements(driver, this);
	}

	
	@FindBy(id = "viewLead_firstName_sp") WebElement elefirstname;
	@FindBy(id = "viewLead_lastName_sp") WebElement elelastname;
	
	public ViewLeadPage verifyfname(String data)
	{
		verifyExactText(elefirstname, data);
		return this;
	}
	public ViewLeadPage verifylname(String data)
	{
		verifyExactText(elelastname, data);
		return this;
	}
	
}
