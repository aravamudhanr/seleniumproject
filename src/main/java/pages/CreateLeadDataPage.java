package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class CreateLeadDataPage extends ProjectMethods{

	public CreateLeadDataPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "createLeadForm_companyName") WebElement elecomname;
	@FindBy(id = "createLeadForm_firstName") WebElement elefname;
	@FindBy(id = "createLeadForm_lastName") WebElement elelname;
	@FindBy(name = "submitButton") WebElement elecreate;
	
	
	public CreateLeadDataPage typecomname(String data)
	{
		type(elecomname, data);
		return this;
	}
	public CreateLeadDataPage typefname(String data)
	{
		type(elefname, data);
		return this;
	}
	public CreateLeadDataPage typelname(String data)
	{
		type(elelname, data);
		return this;
	}
	public ViewLeadPage clickcreate()
	{
		click(elecreate);
		return new ViewLeadPage();
	}

}
