package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class ToLeadPage extends ProjectMethods{

	public ToLeadPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "//input[@name='firstName']")
	WebElement elewintofirstname;
	@FindBy(how = How.XPATH, using = "//button[text()='Find Leads']")
	WebElement eletofindlead;
	@FindBy(how = How.XPATH, using = "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]")
	WebElement eletoselectfirst;	
	/*public void SwitchFromWindow()
	{
		switchToWindow(1);
	}*/
	
	public ToLeadPage TypeToFirstName(String data)
	{
		switchToWindow(1);
		type(elewintofirstname, data);
		return this;
	}
	public ToLeadPage ClickToFindLeads() throws InterruptedException
	{
		click(eletofindlead);
		Thread.sleep(3000);
		return this;
	}
	public MergeLeadDataPage SelectToLeadID()
	{
		click(eletoselectfirst);
		switchToWindow(0);
		return new MergeLeadDataPage();
	}
}
