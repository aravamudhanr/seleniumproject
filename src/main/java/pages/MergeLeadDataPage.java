package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MergeLeadDataPage extends ProjectMethods{

	public MergeLeadDataPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "(//img[@alt='Lookup'])[1]")
	WebElement elefromlead;
	@FindBy(how = How.XPATH, using = "(//img[@alt='Lookup'])[2]")
	WebElement eletolead;
	@FindBy(how = How.XPATH, using = "(//a[text()='Merge']")
	WebElement elemergeclick;
	
	
	public FromFirstLeadPage FromLead()
	{
		click(elefromlead);
		return new FromFirstLeadPage();
	}
	public ToLeadPage ToLead()
	{
		click(eletolead);
		return new ToLeadPage();
		
	}
	public void ClickMerge()
	{
		click(elemergeclick);
	}
}
