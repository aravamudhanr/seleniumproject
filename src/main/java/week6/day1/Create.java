package week6.day1;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import TestCases.BasicLogin;

public class Create extends BasicLogin {
	@BeforeClass//(groups = "common")
	public void setData() {
		// imgFolder = "CreateAccounts";
		testcaseName = "TC01_CreateAccount";
		testDesc = "Create a Account with mandate values";
		author = "Ara";
		category = "Functional Testing";
	}

	// @Test (invocationCount=2)
	@Test (dataProvider="dynamicdata")//(groups = "smoke")
	
	public void createLead(String comname, String fname, String lname) {
		// login();
		click(locateElement("linktext", "Leads"));
		click(locateElement("linktext", "Create Lead"));
		WebElement company = locateElement("id", "createLeadForm_companyName");
		type(company, comname);
		WebElement firstname = locateElement("id", "createLeadForm_firstName");
		type(firstname, fname);
		WebElement lastname = locateElement("id", "createLeadForm_lastName");
		type(lastname, lname);
		/*WebElement fnamelocal = locateElement("id", "createLeadForm_firstNameLocal");
		type(fnamelocal, "Ara");
		WebElement lnamelocal = locateElement("id", "createLeadForm_lastName");
		type(lnamelocal, "R");
		WebElement salution = locateElement("id", "createLeadForm_personalTitle");
		type(salution, "Mr");

		// Drop dwon select
		WebElement source = locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingText(source, "Public Relations");

		WebElement title = locateElement("id", "createLeadForm_personalTitle");
		type(title, "Title");
		WebElement annualrevenue = locateElement("id", "createLeadForm_annualRevenue");
		type(annualrevenue, "Title");

		// Industry dropdown
		WebElement industrydrop = locateElement("id", "createLeadForm_industryEnumId");
		selectDropDownUsingText(industrydrop, "Computer Software");

		WebElement ownership = locateElement("id", "createLeadForm_ownershipEnumId");
		selectDropDownUsingText(ownership, "LLC/LLP");

		// click(locateElement("name", "submitButton"));

		// WebElement verifyfname = locateElement("id", "viewLead_firstName_sp");
		// verifyExactText(verifyfname,"AraTest1");

		System.out.println("All the statements are executed");
		// closebrowse();*/
	}
	@DataProvider(name="dynamicdata")
public String[][] getdata()
{
	String data[][] = new String[2][3];
	data[0][0]="Tata";
	data[0][1]="Ara";
	data[0][2]="R";
	
	data[1][0]="CTS";
	data[1][1]="Raja";
	data[1][2]="M";
	
	return data;
}
}
