package TestCase;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateLead 
{
public ChromeDriver driver;
	@Given("Open the browser")
	public void open_the_browser() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	    driver = new ChromeDriver();
	}

	@Given("Max the browser")
	public void max_the_browser() {
	    driver.manage().window().maximize();
	    
	}

	@Given("Set the TimeOut")
	public void set_the_TimeOut() {
	    driver.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);
	    
	}

	@Given("Launch the URL")
	public void launch_the_URL() {
		driver.get("http://leaftaps.com/opentaps/");
	    
	}

	@Given("Enter the UserName as (.*)")
	public void enter_the_UserName_as_DemoSalesManager(String data) {
		driver.findElementById("username").sendKeys(data);
	    
	}

	@Given("Enter the Password as (.*)")
	public void enter_the_Password_as_crmsfa(String pwd) {
		driver.findElementById("password").sendKeys(pwd);
	}

	@When("Click on the Login Button")
	public void click_on_the_Login_Button() {
		driver.findElementByClassName("decorativeSubmit").click();
	    
	}

	/*@Then("Verify the Login")
	public void verify_the_Login() {
	    // Write code here that turns the phrase above into concrete actions
	    
	}*/

	@And("Click on the CRM Link")
	public void click_on_the_CRM_Link() {
		driver.findElementByLinkText("CRM/SFA").click();
	    
	}

	@And("Click on the Leads")
	public void click_on_the_Leads() {
		driver.findElementByXPath("//a[text()='Leads']").click();
	}

	@And("Click on the Create Lead")
	public void click_on_the_Create_Lead() {
		driver.findElementByLinkText("Create Lead").click();
	}

	@And("Enter the Company Name as (.*)")
	public void enter_the_Company_Name_as_TCS(String comname) {
		driver.findElementById("createLeadForm_companyName").sendKeys(comname);
	    
	}

	@And("Enter the First Name as (.*)")
	public void enter_the_First_Name_as_Ara(String fname) {
		driver.findElementById("createLeadForm_firstName").sendKeys(fname);
	    
	}

	@And("Enter the Last Name as (.*)")
	public void enter_the_Last_Name_as_R(String lname) {
		driver.findElementById("createLeadForm_lastName").sendKeys(lname);
	    
	}

	@When("Click on Create Lead Button")
	public void click_on_Create_Lead_Button() {
		driver.findElementByName("submitButton").click();
	    
	}

	
}
